<?php
echo "<pre>";
$array = array("One", "Two", "Three", 4); # Example of Index array
var_dump($array);


$array = array("01"=>"One", "02"=>"Two", "03"=>"Three", "04"=>4); # Example of Associative array
var_dump($array);

$array = array("01"=>"One", "02"=>"Two", "03"=>array("03"=>"Three", "04"=>4)); # Example of Multi-dimensinal array
var_dump($array);
echo "</pre>";
?>